<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">

    <title>MarkerEvent</title>

</head>
<body>
<div id="main-map"></div>
<div class="side-panel">
    <!--  Menu de changement de theme pour la Map  -->
    <div id="menu">
        <input id="satellite-streets-v11" type="radio" name="rtoggle" value="satellite">
        <label for="satellite-streets-v11">Satellite</label>
        <input id="light-v10" type="radio" name="rtoggle" value="light">
        <label for="light-v10">Light</label>
        <input id="dark-v10" type="radio" name="rtoggle" value="dark">
        <label for="dark-v10">Dark</label>
        <input id="streets-v11" type="radio" name="rtoggle" value="streets">
        <label for="streets-v11">Streets</label>
        <input id="outdoors-v11" type="radio" name="rtoggle" value="outdoors" checked="checked">
        <label for="outdoors-v11">Outdoors</label>
    </div>
    <h1>MarkerEvent</h1>
    <form class="form-group" novalidate>
        <!--    Formulaire pour créer les marker    -->
        <div class="form-group">
            <label>Nom de l'évenement
                <input type="text" class="form-control" id="new-event-title">
            </label>
        </div>
        <div class="form-group">
            <label>Description
                <textarea class="form-control form-control-lg" rows="3" id="new-event-content"></textarea>
            </label>
        </div>
        <div class="form-group">
            <label>Longitude
                <input type="number" class="form-control" id="new-event-lon" disabled>
            </label>
        </div>
        <div class="form-group">
            <label>Latitude
                <input type="number" class="form-control" id="new-event-lat" disabled>
            </label>
        </div>
        <div class="form-group">
            <label>Début de l'évenement
                <input type="datetime-local" class="form-control" id="new-event-start">
            </label>
        </div>
        <div class="form-group">
            <label>Fin de l'évenement
                <input type="datetime-local" class="form-control" id="new-event-end">
            </label>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-info mt-3" value="Ajouter">
        </div>
    </form>
</div>
</body>
</html>