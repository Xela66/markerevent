import config from '../../app.config';

import 'mapbox-gl/dist/mapbox-gl.css';
import mapboxgl from 'mapbox-gl';
import MapboxGeocoder from '@mapbox/mapbox-gl-geocoder';
import '@mapbox/mapbox-gl-geocoder/dist/mapbox-gl-geocoder.css';

import '../../assets/css/reset.css';
import '../../assets/css/style.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import '@fortawesome/fontawesome-free/js/fontawesome';
import '@fortawesome/fontawesome-free/js/solid';
import '@fortawesome/fontawesome-free/js/regular';
import '@fortawesome/fontawesome-free/js/brands';

import {LocalStorageService} from "./Service/LocalStorageService";
import {MarkerEvent} from "./Entity/MarkerEvent";


import {RefreshControl} from "./Control/RefreshControl";
import {DeleteControl} from "./Control/DeleteControl";

const STORAGE_MARKER_KEY = 'marker-key';

/**
 * Initialise les éléments du DOM de l'application
 */
class App {

    mainMap = null;
    geocoder;

    arrMarkers;

    constructor() {

        this.markerKeyStorage = new LocalStorageService(STORAGE_MARKER_KEY);

        this.elForm = document.forms[0];
        this.elTitle = document.getElementById('new-event-title');
        this.elContent = document.getElementById('new-event-content');
        this.elDateStart = document.getElementById('new-event-start');
        this.elDateEnd = document.getElementById('new-event-end');
        this.elLatitude = document.getElementById('new-event-lat');
        this.elLongitude = document.getElementById('new-event-lon');
        this.elForm.addEventListener('submit', this.handlerMarkerNew.bind(this));

        mapboxgl.accessToken = config.mapbox.token;
    }

    /**
     * Démarre l'application
     */
    start() {

        this.arrMarkers = [];

        console.info('Starting App...');

        this.mainMap = new mapboxgl.Map({
            container: 'main-map',
            style: 'mapbox://styles/mapbox/outdoors-v11'
        });

        const layerList = document.getElementById('menu');
        const inputs = layerList.getElementsByTagName('input');

        for (const input of inputs) {
            input.onclick = (layer) => {
                const layerId = layer.target.id;
                this.mainMap.setStyle('mapbox://styles/mapbox/' + layerId);
            };
        }

        // Ajout du control Geocoder sur la Map.
        this.geocoder = new MapboxGeocoder({
            accessToken: mapboxgl.accessToken,
            mapboxgl: mapboxgl
        });

        this.mainMap.addControl(this.geocoder);


        // Ajout des valeurs latitude et longitude dans l'input html au click
        this.mainMap.on('click', (evt) => {
            this.elLatitude.value = evt.lngLat.lat;
            this.elLongitude.value = evt.lngLat.lng;
        });

        // Ajout du contrôle de navigation (zoom, inclinaison, etc.)
        const navControl = new mapboxgl.NavigationControl({
            visualizePitch: true
        });
        this.mainMap.addControl(navControl, 'bottom-right');

        // Ajout du contrôle de Géolocalisation
        const geoLocControl = new mapboxgl.GeolocateControl({
            fitBoundsOptions: {
                maxZoom: 17.5
            },
            positionOptions: {
                enableHighAccuracy: true
            },
            showUserHeading: true,
            // trackUserLocation: true
        });

        this.mainMap.addControl(geoLocControl, 'top-right');

        // Ajout d'un controle personnalisé "RefreshControl" pour rafraichir la page
        const refreshControl = new RefreshControl();
        this.mainMap.addControl( refreshControl, 'top-left' );

        // Ajout d'un controle personnalisé "DeteleControl" qui supprime le localStorage
        const deleteControl = new DeleteControl();
        this.mainMap.addControl( deleteControl, 'bottom-left');

        this.renderMarkerKey();
    }

    renderMarkerKey(){
        let itemStorageMarker = this.markerKeyStorage.getJSON();

        if ( itemStorageMarker === null ) itemStorageMarker = [];

        for (let itemMarker of itemStorageMarker ) this.arrMarkers.push( new MarkerEvent( itemMarker, this.mainMap ) );
        for (let arrMarker of this.arrMarkers ){
        }
    }

    // Formulaire du création de Marker
    handlerMarkerNew( evt ) {
        evt.preventDefault();
        if (this.geocoder.mapMarker) {
            this.elLatitude.value = this.geocoder.mapMarker._lngLat.lat;
            this.elLongitude.value = this.geocoder.mapMarker._lngLat.lng;
            this.geocoder.mapMarker = null;
        }
        let
            hasError = false,
            regAlphaNum = new RegExp('^[A-Za-z0-9 ]+$'),
            strTitle = this.elTitle.value.trim(),
            strContent = this.elContent.value.trim(),
            NumLatitude = parseFloat(this.elLatitude.value.trim()),
            NumLongitude = parseFloat(this.elLongitude.value.trim()),
            strDateStart = this.elDateStart.value,
            strDateEnd = this.elDateEnd.value;

        if (!regAlphaNum.test(strTitle)) {
            hasError = true;
            this.elTitle.value = '';
            this.elTitle.classList.add('error');
        }
        if (strContent.length <= 0) {
            hasError = true;
            this.elContent.classList.add('error');
        }
        if (hasError) return;
        this.elTitle.value
            = this.elContent.value
            = '';

        const newEventMarker = {
            title: strTitle,
            content: strContent,
            dateStart: new Date(strDateStart),
            dateEnd: new Date(strDateEnd),
            latitude: NumLatitude,
            longitude: NumLongitude
        };
        this.arrMarkers.push(new MarkerEvent(newEventMarker, this.mainMap));
        this.markerKeyStorage.setJSON(this.arrMarkers);
    }
}

const instance = new App();

export default instance;