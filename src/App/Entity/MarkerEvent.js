import mapboxgl, {Marker} from "mapbox-gl";

export class MarkerEvent {
    dateStart;
    dateEnd;
    title;
    content;
    longitude;
    latitude;

    container = null;
    markerPop;

    constructor(json, mainMap) {
        this.dateStart = json.dateStart;
        this.dateEnd = json.dateEnd;
        this.title = json.title;
        this.content = json.content;
        this.latitude = json.latitude;
        this.longitude = json.longitude;

        const markerWPop = new Marker( // création d'un nouveau Marker
            {
                color: this.handlerColorDate()
            }
        );
        markerWPop.setLngLat({ // récupération des coordonnées dans le formulaire
            lat: json.latitude,
            lon: json.longitude
        });

        markerWPop.addTo(mainMap);

        // Construction du Marker sur la Map
        let dateStart = new Date(json.dateStart).toLocaleDateString();
        let dateEnd = new Date(json.dateEnd).toLocaleDateString();
        let hoursStart = new Date(json.dateStart).getHours();
        let minutesStart = new Date(json.dateStart).getMinutes();
        let hoursEnd = new Date(json.dateEnd).getHours();
        let minutesEnd = new Date(json.dateEnd).getMinutes();
        let message = this.handlerMarkerMessage();
        const popup =
            new mapboxgl.Popup();

        // Affiche les debut et fin de dates des evenements
        if (minutesStart && minutesStart < 10) {
            popup.setHTML(`
            <div class="new-popup-event">
               <h3>${json.title}</h3>
               <p>${json.content}</p>
               <ol>
                    <li>du ${dateStart} à ${hoursStart}H0${minutesStart}
                     au ${dateEnd} à ${hoursEnd}H0${minutesEnd}</li>
                    <li>${message}</li>
                </ol>
            </div>`);
        } else {
            popup.setHTML(`
            <div class="new-popup-event">
               <h3>${json.title}</h3>
               <p>${json.content}</p>
               <ol>
                    <li>du ${dateStart} à ${hoursStart}H${minutesStart}
                     au ${dateEnd} à ${hoursEnd}H${minutesEnd}</li>
                    <li>${message}</li>
                </ol>
            </div>`);
        }

        markerWPop.setPopup(popup);
        markerWPop.addTo(mainMap);

        const markerDiv = markerWPop.getElement();
        markerDiv.title = `${this.title} du ${dateStart} au ${dateEnd}`;
    }

    // Fonction pour les dates des Marker
    handlerColorDate() {
        let dateThreeDayBefore = new Date(this.dateStart).getTime() - (3 * 24 * 60 * 60 * 1000);
        let dateNow = Date.now();
        let dateEnd = new Date(this.dateEnd).getTime();

        if (dateNow < dateThreeDayBefore) {
            return '#28a745'
        } else if (dateEnd < dateNow) {
            return '#dc3545'
        } else {
            return '#ff6700';
        }
    }

    // Afficher les message des PopUp
    handlerMarkerMessage() {
        let dateThreeDayBefore = new Date(this.dateStart).getTime() - (3 * 24 * 60 * 60 * 1000);
        let dateNow = Date.now();
        let dateEnd = new Date(this.dateEnd).getTime();
        let dateStart = new Date(this.dateStart).getTime();
        let diff_date = dateStart - dateNow;
        let dateHours = new Date(this.dateStart).getHours() - new Date(dateNow).getHours();
        let dateMin = new Date(this.dateStart).getMinutes() - new Date(dateNow).getMinutes();
        let diff_days = Math.round(diff_date / (1000 * 3600 * 24));
        // Condition pour afficher les dates, heures et minutes des PopUp
        if (dateNow < dateThreeDayBefore) {
            return `Prochainement`;
        } else if (dateEnd < dateNow) {
            return `Quel dommage ! vous avez raté cet événement`;
        } else if (diff_days > 0) {
            return `Attention, l'évènement commence dans ${diff_days} jour(s) et ${Math.round(dateHours)} heure(s)`;
        } else if (dateHours > 0 && diff_days <= 0 && dateNow <= dateStart) {
            return `Attention, l'évènement commence dans ${Math.abs(dateHours)} heure(s)`;
        } else if (dateHours >= 0 && diff_days <= 0 && dateNow <= dateStart) {
            if (Math.round(dateMin) < 10) {
                return `Attention, l'évènement commence dans 0${Math.round(dateMin)} minutes(s)`;
            } else {
                return `Attention, l'évènement commence dans ${Math.round(dateMin)} minutes(s)`;
            }
        } else {
            return `L'évènement ${this.title} est en cours`;
        }

    }
}

