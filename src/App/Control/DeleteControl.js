export class DeleteControl {
    map;
    container;

    onAdd(map) {
        this.map = map;
        this.container = document.createElement('div');
        this.container.classList.add('mapboxgl-ctrl', 'mapboxgl-ctrl-group');
        this.container.innerHTML =
            '<button type="button" class="map-control-delete">' +
            '<span><i class="fa-solid fa-trash"></i></span>' +
            '</button>';

        this.container.children[0].addEventListener('click', this.handlerDeleteClick.bind(this));

        return this.container;
    }

    onRemove() {
        // Nettoyage
        this.container.removeEventListener(this.handlerDeleteClick); // Suppression des écouteurs
        this.container.remove(); // Suppression de l'élément de l'arbre DOM
        this.container = undefined; // Suppression de la référence (pour que Garbage collector vide la mémoire)
        this.map = undefined; // Suppression la référence
    }

    handlerDeleteClick() { // Fonction pour supprimer les Markers sur la Map
        localStorage.clear(); // Suppression du local storage
        location.reload(); // Rechargement de la page apres le clear
    }
}