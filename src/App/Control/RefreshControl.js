export class RefreshControl {
    map;
    container;

    onAdd(map) {
        this.map = map;
        this.container = document.createElement('div');
        this.container.classList.add('mapboxgl-ctrl', 'mapboxgl-ctrl-group');
        this.container.innerHTML =
            '<button type="button" class="map-control-refresh">' +
                '<span><i class="fa-solid fa-rotate-right"></i></span>' +
            '</button>';

        this.container.children[0].addEventListener('click', this.handlerRefreshClick.bind(this));

        return this.container;
    }

    onRemove() {
        // Nettoyage
        this.container.removeEventListener(this.handlerRefreshClick); // Suppression des écouteurs
        this.container.remove(); // Suppression de l'élément de l'arbre DOM
        this.container = undefined; // Suppression de la référence (pour que Garbage collector vide la mémoire)
        this.map = undefined; // Suppression la référence
    }

    handlerRefreshClick() { // Fonction pour recharger la Map
        location.reload();
    }
}